echo "Getting instance IP..."
export LOCAL_IP=$(curl --retry 5 --connect-timeout 3 -s "169.254.169.254/latest/meta-data/local-ipv4")
echo "Complete! ${LOCAL_IP}"
exec "$@"
