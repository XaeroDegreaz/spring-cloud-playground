package com.dobydigital.springcloudplayground.restapplication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableEurekaClient
@RestController
@RequestMapping( "/api/rest-application" )
@SpringBootApplication
public class RestApplicationApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run( RestApplicationApplication.class, args );
    }

    @Value( "${instance-name}" )
    private String instanceName;

    @GetMapping
    public String get()
    {
        return instanceName;
    }
}
